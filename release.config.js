module.exports = {
  repositoryUrl: "https://gitlab.com/juangui9696/semantic.git",
  verifyConditions: [
    'semantic-release-git-branches',
  ],
  analyzeCommits: [[
    '@semantic-release/commit-analyzer',
    {
      preset: 'angular'
    }
  ]],
  verifyRelease: [],
  generateNotes: ['@semantic-release/release-notes-generator'],
  prepare: [
    'semantic-release-expo',
    {
      path: 'semantic-release-git-branches',
      assets: [
        'package.json',
        'CHANGELOG.md',
      ],
      branchMerges: [
        'master',
      ],
      message: 'chore: create new release ${nextRelease.version}\n\n${nextRelease.notes}'
    }
  ],
  publish: [],
  fail: [],
  success: [],
  addChannel: []
}